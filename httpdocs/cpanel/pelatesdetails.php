<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form")) {
  $insertSQL = sprintf("INSERT INTO products (PELATESID, EIDOS, typos, XRWMA, PAXOS, SYSKEVASIA, mon_metrisis, SHMEIWSH, imerominia, product_tympano) VALUES (".$_GET['pelatesid'].", %s, %s, %s, %s, %s, %s, %s, curdate(), %s)",
                       GetSQLValueString($_POST['eidos'], "text"),
					   GetSQLValueString($_POST['typos'], "text"),
                       GetSQLValueString($_POST['xrwma'], "text"),
                       GetSQLValueString($_POST['paxos'], "text"),
                       GetSQLValueString($_POST['syskevasia'], "text"),
					   GetSQLValueString($_POST['mon_metrisis'], "text"),
                       GetSQLValueString($_POST['shmeiwsh'], "text"),
                       GetSQLValueString($_POST['product_tympano'], "text"));

  mysql_select_db($database_con1, $con1);
  $Result1 = mysql_query($insertSQL, $con1) or die(mysql_error());
  	$lastid= mysql_insert_id();
   $insertSQL = sprintf("INSERT INTO entoles_ext (product_id_fk, PlastType, PlastMaterial, PlastInfo, Weight, Info_ext, Plast_Syskevasia, product_printed) VALUES ($lastid, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['PlastType'], "text"),
					   GetSQLValueString($_POST['PlastMaterial'], "text"),
                       GetSQLValueString($_POST['PlastInfo'], "text"),
                       GetSQLValueString($_POST['Weight'], "text"),
                       GetSQLValueString($_POST['Info_ext'], "text"),
					   GetSQLValueString($_POST['Plast_Syskevasia'], "text"),
					   GetSQLValueString($_POST['product_printed'], "int"));

  mysql_select_db($database_con1, $con1);
  $Result1 = mysql_query($insertSQL, $con1) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "klise")) {
  $insertSQL = sprintf("INSERT INTO klise (PELATESID, klise_number, klise_name) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['PELATESID'], "int"),
                       GetSQLValueString($_POST['klise_number'], "int"),
                       GetSQLValueString($_POST['klise_name'], "text"));

  mysql_select_db($database_con1, $con1);
  $Result1 = mysql_query($insertSQL, $con1) or die(mysql_error());
}

$colname_rs_pelDetails = "-1";
if (isset($_GET['pelatesid'])) {
  $colname_rs_pelDetails = $_GET['pelatesid'];
}
mysql_select_db($database_con1, $con1);
$query_rs_pelDetails = sprintf("SELECT * FROM products RIGHT JOIN pelates ON products.pelatesid=pelates.pelatesid LEFT JOIN entoles_ext ON products.proionID=entoles_ext.product_id_fk WHERE pelates.PELATESID = %s", GetSQLValueString($colname_rs_pelDetails, "int"));
$rs_pelDetails = mysql_query($query_rs_pelDetails, $con1) or die(mysql_error());
$row_rs_pelDetails = mysql_fetch_assoc($rs_pelDetails);
$totalRows_rs_pelDetails = mysql_num_rows($rs_pelDetails);

$colname_rs_klise = "-1";
if (isset($_GET['pelatesid'])) {
  $colname_rs_klise = $_GET['pelatesid'];
}
mysql_select_db($database_con1, $con1);
$query_rs_klise = sprintf("SELECT * FROM klise WHERE PELATESID = %s ORDER BY klise_number DESC", GetSQLValueString($colname_rs_klise, "int"));
$rs_klise = mysql_query($query_rs_klise, $con1) or die(mysql_error());
$row_rs_klise = mysql_fetch_assoc($rs_klise);
$totalRows_rs_klise = mysql_num_rows($rs_klise);

mysql_select_db($database_con1, $con1);
$query_rs_last = sprintf("SELECT MAX(klise_number) as lastid FROM klise");
$rs_last = mysql_query($query_rs_last, $con1) or die(mysql_error());
$row_rs_last = mysql_fetch_assoc($rs_last);
$totalRows_rs_last = mysql_num_rows($rs_last);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Civil | Καρτέλα Πελάτη - Νέο Προϊόν </title>
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
			setSidebarHeight();


        });
    </script>
</head>
<body>
    <div class="container_12">
        <?php include("header.php"); ?>
        
        <div class="clear">
        </div>
      <?php include("menu.php"); ?>
        <div class="clear">
        </div>
        <div class="grid_12">
            <div class="box round first grid">
                <h2>
                    Πελάτης: <?php echo $row_rs_pelDetails['PELNAME']; ?></h2>
                <div class="block">
                    
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="newParaggeliaProin.php?PELATESID=<?php echo $row_rs_pelDetails['PELATESID']; ?>" > <b><input class="btn btn-purple"  name="New Order" type="submit" value="Νεα Παραγγελία"/></b></a><br /><br />
                	<table border="1" width="100%">
                    	<tr>
                    <?php $i=1; do { 
						$bg= "#FFF".rand(100,999);
					?>
                        	<td bgcolor="<?php echo $bg; ?>">
                                                  
            	            	<a class="btn-mini btn-red btn-cross" href="deleteproion.php?proionID=<?php echo $row_rs_pelDetails['proionID']; ?>&pelatesid=<?php echo $row_rs_pelDetails['PELATESID']; ?>" onclick="return confirm('Είσαι σίγουρος?');" ><span></span>Delete</a>                        
                        <a class="btn-mini btn-black btn-check" href="editProion.php?proionID=<?php echo $row_rs_pelDetails['proionID']; ?>&pelatesid=<?php echo $row_rs_pelDetails['PELATESID']; ?>"><span></span>Edit</a><br />
                        
                        
                        <b>Προϊόν:</b> <?php echo $row_rs_pelDetails['EIDOS']; ?><br />
                        <b>Τύπος:</b> <?php echo $row_rs_pelDetails['typos']; ?><br />
                        <b>Χρώμα Υλικού:</b> <?php echo $row_rs_pelDetails['XRWMA']; ?><br />
                        <b>Πάχος: </b><?php echo $row_rs_pelDetails['PAXOS']; ?><br />
                        <!--Out by gina
                        <b>Συσκευασία:</b> <?php echo $row_rs_pelDetails['SYSKEVASIA']; ?><br />
                        <b>Μονάδα Μέτρησης:</b> <?php echo $row_rs_pelDetails['mon_metrisis']; ?><br />
                        -->
                        <b>Λεπτομέρειες:</b> <?php echo $row_rs_pelDetails['SHMEIWSH']; ?><br />
                        <b>Τύμπανο </b><?php echo $row_rs_pelDetails['product_tympano']; ?><br />
                        <font size="1px" color="#FF3366"><b><?php echo $row_rs_pelDetails['imerominia']; ?></b></font><br />
                 		<br />
                        </td>
                        <td bgcolor='<?php echo $bg; ?>'>
                        
                        
                       <br /> 
                       <b>Περιγραφή:</b><?php echo $row_rs_pelDetails['PlastType']; ?><br />
                       <b>Υλικό:</b><?php echo $row_rs_pelDetails['PlastMaterial']; ?> <br />
                       <b>Συνταγή:</b><?php echo $row_rs_pelDetails['PlastInfo']; ?> <br />
                       <b>Συσκευασία:</b><?php echo $row_rs_pelDetails['Plast_Syskevasia']; ?> <br />
                       <b>Βάρος:</b><?php echo $row_rs_pelDetails['Weight']; ?><br />
                       <b>Εκτυπωμένο:</b><?php if($row_rs_pelDetails['product_printed']==1) echo "NAI"; elseif($row_rs_pelDetails['product_printed']==0) echo "OXI"; ?><br />
                       <b>Προσοχή:</b><?php echo $row_rs_pelDetails['Info_ext']; ?><br />
                       </td>
                       <?php if($i==3) { echo '</tr><tr height="2px" ><td></td><td></td><td></td></tr><tr>'; $i=1; } else { $i++; } ?>
                        
                    <?php
						$bg= "#FFF".rand(100,999); } while ($row_rs_pelDetails = mysql_fetch_assoc($rs_pelDetails)); ?>
                    	</tr>
                    </table>
                    
                  <br /><h2>Νέο Προιόν</h2>
                  <form method="POST" name="form" action="<?php echo $editFormAction; ?>">
                  <table>
                  <tr>
                  	<td>
                    <table width="200" border="1" >
                     <tr>
                        <th scope="row">Είδος  </th>
                        <td><input name="eidos" type="text" required="required" /></td>
                      
                    
                      </tr>
                      <tr>
                        <th scope="row">Τύπος  </th>
                        <td><select name="typos" id="typos">
                            <option value="ΤΣΑΝΤΑ">ΤΣΑΝΤΑ</option>
                            <option value="ΦΥΛΛΟ">ΦΥΛΛΟ</option>
                            <option value="ΣΑΚΟΥΛΑΚΙ ΔΙΑΦΑΝΕΣ">ΣΑΚΟΥΛΑΚΙ ΔΙΑΦΑΝΕΣ</option>
                            <option value="ΣΩΛΗΝΑΣ">ΣΩΛΗΝΑΣ</option>
                            <option value="ΣΑΚΟΣ">ΣΑΚΟΣ</option>
                        </select></td>
                      </tr>
                      <tr>
                        <th scope="row">Χρώμα</th>
                        <td><input name="xrwma" type="text" class="error" /></td>
                      </tr>
                      <tr>
                        <th scope="row">Πάχος;</th>
                        <td><input name="paxos" type="text" class="success" /></td>
                      </tr>
                      <!--OUT BY GINA
                      <tr>
                        <th scope="row">Συσκευασία</th>
                        <td><input name="syskevasia" type="text" /></td>
                      </tr>
                      <tr>
                        <th scope="row">Μονάδα Μέτρησης</th>
                        <td><input name="mon_metrisis" type="text"  required="required" /></td>
                      </tr>
                      -->
                      <tr>
                        <th scope="row">Λεπτομέρειες</th>
                        <td><textarea name="shmeiwsh" cols="40" rows="5"></textarea></td>
                      </tr>
                      <tr>
                        <th scope="row">Μέγεθος Τυμπάνου</th>
                        <td>
                            <select name="product_tympano" required="required" >
                                <option></option>
                                <option value="0">Ατύπωτο</option>
                                <option>30x</option>
                                <option>35x</option>
                                <option>40x</option>
                                <option>45x</option>
                                <option>50x</option>
                                <option>55x</option>
                                <option>60x</option>
                                <option>65x</option>
                                <option>70x</option>
                                <option>75x</option>
                                <option>80x</option>
                                <option>85x</option>
                            </select>
                        
                        </td>
                      </tr> 
                    </table>
					</td>
                    <td>
                             <table width="200" border="1">
                             <tr>
                                <th scope="row">Extruder</th>
                                <td><input name="PlastType" type="text" required="required" /></td>
                              </tr>
                              <tr>
                                <th scope="row">Υλικό </th>
                                <td><input name="PlastMaterial" type="text" /></td>
                              </tr>
                              <tr>
                                <th scope="row">Συνταγή</th>
                                <td><input name="PlastInfo" type="text" class="error" /></td>
                              </tr>
                              <tr>
                                <th scope="row">Συσκευασία</th>
                                <td><input name="Plast_Syskevasia" type="text" /></td>
                              </tr>
                               <tr>
                                <th scope="row">Βαρος Δεματος</th>
                                <td><input name="Weight" type="text" class="success" /></td>
                              </tr>
                               <tr>
                                <th scope="row">Έκτυπωμένο </th>
                                <td> ΝΑΙ<input name="product_printed" type="radio" class="success" value="1" checked="checked"/> ΌΧΙ<input name="product_printed" type="radio" class="success" value="0" /></td>
                              </tr>
                              <tr>
                                <th scope="row">Λεπτομέρειες</th>
                                <td><textarea name="Info_ext" cols="40" rows="5"></textarea></td>
                              </tr>
                                
                              </tr>
                            </table>
                    
                    
                    </td>
                    </tr>
                    </table>


                    <input class="btn btn-red"  name="" type="submit" />
                    
                    <input type="hidden" name="MM_insert" value="form" />
                  </form>
                </div>
            </div>
        </div>
      <div class="grid_12">
            <div class="box round first grid">
                
                 <h2>Νέο Κλισέ</h2>
                <form action="<?php echo $editFormAction; ?>" method="POST"name="klise"  >
                	Αριθμός Κλισε:<input type="text" name="klise_number" value="<?php echo $row_rs_last['lastid']+1; ?>" required="required"/><br />
                    Όνομα Κλισέ:<input type="text" name="klise_name" required="required" /><br />
                    <input type="hidden" name="PELATESID" value="<?php echo $colname_rs_pelDetails; ?>"/>
                   <input type="submit" value="Δημιουργία" class="btn btn-red">
                   <input type="hidden" name="MM_insert" value="klise" />
                </form>
                <br /> <br />
                
                <h2>Κλισέ</h2>
                <div class="block">
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Αριθμός Κλισέ</th>
						  	<th>Όνομα Κλισε</th>
						  	<th>Ενέργειες</th>
						</tr>
					</thead>
					<tbody>
					      <?php do { ?>
                            <tr>
                                <td class="center"><?php echo $row_rs_klise['klise_number']; ?></td>
                                <td class="center"><?php echo $row_rs_klise['klise_name']; ?></td>
                                <td>
                                	<a class="btn-mini btn-red btn-cross" href="deleteproion.php?klise_number=<?php echo $row_rs_klise['klise_number']; ?>&pelatesid=<?php echo $row_rs_klise['PELATESID']; ?>&klise_name=<?php echo $row_rs_klise['klise_name']; ?>" onclick="return confirm('Είσαι σίγουρος?');" ><span></span>Delete</a>
                                </td>
                            </tr> 
					        <?php } while ($row_rs_klise = mysql_fetch_assoc($rs_klise)); ?>
                    </tbody>
				</table>
                  <div class="clear">
       			 </div>
                </div>
            </div>
        </div>
      <div class="clear">
      </div>
    </div>
    <div class="clear">
    </div>
    <?php include("footer.php"); ?>
</body>
</html>
<?php
mysql_free_result($rs_pelDetails);

mysql_free_result($rs_klise);

?>
