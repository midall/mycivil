<?php
require_once( 'validation.php' );
require_once( 'Connections/con1.php' );
mysql_select_db( $database_con1, $con1 );

if( isset( $_POST['printsak'] ) && isset( $_POST['job'] ) )
{
	$box = $_POST['job'];
	$conStr = implode( ',', $box );
	
	$query_sak = 'SELECT sak FROM paraggelia WHERE paraggelia.orderID in (' . $conStr . ')';
	$result_sak = mysql_query( $query_sak, $con1 ) or die( mysql_error() );
	$row_sak = mysql_fetch_assoc( $result_sak );
	$totalRows_sak = mysql_num_rows( $result_sak );
}
else
{
	$deleteSQL = sprintf( 'TRUNCATE TABLE printorder_sak' );
	mysql_query( $deleteSQL, $con1 ) or die( mysql_error() );
	
	// Redirect
	header( 'Location: index.php' );
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Σακουλομηχανή <?php echo $row_sak['sak'] . ' ' . date( 'd-M-Y' ); ?></title>

	<style type="text/css">
	#footer
	{
		position: absolute; bottom: 0;
	}
	@media print {
		html, body {
		height: 100%;
		margin: 0;
		padding: 0;
		}
	}
	table.order
	{
		border-collapse:collapse;
		border-bottom: 3px solid black; 
	}
	tr.order
	{
		border-bottom: 3px solid black;
	}
	td.order
	{
		padding-top:27px;
		
	}
	textarea
	{
		font-size: 20pt;
		font-family: Arial;
	} 
	</style>
</head>

<body>
<?php
$box = $_POST['job'];
$conStr = implode( ',', $box );

// Get the orders
$query_rs = 'SELECT * ';
$query_rs .= 'FROM paraggelia ';
$query_rs .= 'JOIN products ON paraggelia.proionID_FK = products.proionID ';
$query_rs .= 'JOIN pelates ON paraggelia.PELATESID = pelates.PELATESID ';
$query_rs .= 'JOIN entoles_ext ON products.proionID = entoles_ext.product_id_fk ';
$query_rs .= 'LEFT JOIN printorder_sak ON paraggelia.ORDERID = printorder_sak.order_id_fk ';
$query_rs .= 'WHERE paraggelia.orderID IN ( ' . $conStr . ') AND paraggelia.status <> 0 ';
$query_rs .= 'ORDER BY count_order ASC';

$result_rs = mysql_query( $query_rs, $con1 ) or die( mysql_error() );
$totalRows_rs = mysql_num_rows( $result_rs );	
?>
<table width="900px">
	<tr>
		<td><img src="img/civillogo.png" height="60" width="150" /><td>
		<td align="right"><br /><br />Ημερομηνία Έκδοσης:&nbsp;&nbsp;<b><?php echo date( 'D,d/M/Y' ); ?></b></td>
	</tr>
</table>

<table width="900px">
	<tr>
		<td align="center" valign="bottom" height="20px" >	Σακουλομηχανή:<b><font size="+3"> <?php echo $row_sak['sak']; ?><b></font></td>
	</tr>
</table>

<table width="900px" class="order">
	<?php 
	$i=1;
	while( $row_rs = mysql_fetch_assoc( $result_rs ) ) { ?>
	<tr class="top">
		<td class="order" style="border-right:#000 solid thin; border-bottom:#000 solid thin;"><b><font size="+1"><?php echo $i.") ".$row_rs['POSOTITA']; ?>&nbsp;κιλά&nbsp; <?php echo $row_rs['PlastType']; ?><br /><?php echo $row_rs['EIDOS']; ?>, <?php echo $row_rs['typos']; ?></b></font></td>
		<td style="border-right:#000 solid thin; border-bottom:#000 solid thin;"><font size="+1"><strong>Χρώμα: </strong><?php echo $row_rs['XRWMA']; ?></font><br /></td>
		<td colspan="2" style="border-bottom:#000 solid thin;"><font size="+1"><strong>Πάχος: </strong><?php echo $row_rs['PAXOS']; ?> &nbsp;&nbsp;&nbsp;<strong>Τύμπανο:</strong></font><?php echo $row_rs['product_tympano']; ?><br /></td>
	</tr>
	<tr>
		<td class="order" style="border-right:#000 solid thin; border-bottom:#000 solid thin;"><font size="+1"><strong>Υλικό:</strong><?php echo $row_rs['PlastMaterial']; ?>&nbsp;<?php echo $row_rs['PlastInfo']; ?></font></td>
		<td class="order" style="border-right:#000 solid thin; border-bottom:#000 solid thin;" colspan="2"><font size="+1"><strong>Συσκευασία:</strong><?php echo $row_rs['Plast_Syskevasia']; ?></font></td>
		<td class="order" style="border-bottom:#000 solid thin;"> <strong>Βάρος Δέματος:</strong><?php echo $row_rs['Weight']; ?></td>
	</tr>
	<tr>
		<td colspan="2" class="order" bgcolor="#CCFFFF" style="border-bottom:#000 solid thin; border-right:#000 solid thin;"><font size="+2"><?php echo $row_rs['Info_ext']; ?></font></td>
		<td class="order" style="border-bottom:#000 solid thin; border-right:#000 solid thin;"><font size="+1"><strong>Εκτύπωση:</strong></font><?php if($row_rs['product_printed']==1) echo "NAI"; elseif($row_rs_pelDetails['product_printed']==0) echo "OXI"; ?><br /></td>
		<td class="order" style="border-bottom:#000 solid thin;"><b><font size="+2"><strong>Πελάτης:</strong><?php echo $row_rs['PELNAME']; ?></font></b></td>
	</tr>
	<?php
	$i++;
	} ?>
</table>

<div id="footer">
	<table width="900px">
		<tr>
			<td>CivilPlastics</td>
			<td align="center">E.05.05/03-02-20<td>
			<td align="right">TUV HELLAS</td>
		</tr>
	</table>
</div>
<?php
$deleteSQL = sprintf( 'TRUNCATE TABLE printorder_sak' );
mysql_select_db( $database_con1, $con1 );
mysql_query( $deleteSQL, $con1 ) or die( mysql_error() );
?>
</body>
</html>
<?php
mysql_free_result( $rs );
?>
