<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form")) {
  if($_POST['cus_status']==5) {
  $insertSQL = sprintf("INSERT INTO pelates (PELNAME, cus_status) VALUES (%s, %s)",
                       GetSQLValueString($_POST['pelname'], "text"),
                       GetSQLValueString($_POST['cus_status'], "int"));
  } else {
	  $insertSQL = sprintf("INSERT INTO pelates (PELNAME) VALUES (%s)",
                       GetSQLValueString($_POST['pelname'], "text"));
  }

  mysql_select_db($database_con1, $con1);
  $Result1 = mysql_query($insertSQL, $con1) or die(mysql_error());
	$lastid= mysql_insert_id();
  $insertGoTo = "pelatesdetails.php?pelatesid=$lastid";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Civil | Νέος Πελάτης</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
			setSidebarHeight();


        });
    </script>
</head>
<body>
    <div class="container_12">
        <?php include("header.php"); ?>
        
        <div class="clear">
        </div>
      <?php include("menu.php"); ?>
        <div class="clear">
        </div>
        
        <div class="grid_12">
            <div class="box round first grid">
                <h2>Εισαγωγή νέου πελάτη</h2>
                <div class="block ">
                  <form method="POST" action="<?php echo $editFormAction; ?>" name="form">
                    <table class="form">
                        <tr>
                            <td class="col1">
                                <label>
                                    Όνομα</label>
                            </td>
                            <td class="col2">
                                <input type="text" id="pelname" name="pelname" required="required" />
                                
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="col1">
                                <label>
                                    Αποθήκη</label>
                            </td>
                            <td class="col2">
                                <input type="checkbox" id="cus_status" name="cus_status" value="5" />
                                <!--5 s for storage -->
                            </td>
                         
                           
                        <tr>
                      	  <td>
                       			 <input name="" type="submit" />
                     	   </td>
                        </tr>
                    </table>
                    <input type="hidden" name="MM_insert" value="form" />
                    </form>
                </div>
            </div>
        </div>
      <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("footer.php"); ?>
</body>
</html>

