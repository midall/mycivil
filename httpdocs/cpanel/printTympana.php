<?php require_once('Connections/con1.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php require_once('validation.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ektypotiki<?php echo date('d-M-Y'); ?></title>

<style type="text/css">
#footer {
    position: absolute; bottom: 0;
}
@media print {
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
}
	table
	{
	border-collapse:collapse;
	}
	textarea {
	   font-size: 20pt;
	   font-family: Arial;
	} 
	.top {
		border-top:thin solid;
		border-color:black;
		padding-bottom:1em;
	}
	
	.bottom {
		border-bottom:thin solid;
		border-color:black;
		padding-bottom:1em;
	}
	
	.left {
		border-left:thin solid;
		border-color:black;
	}
	
	.right {
		border-right:thin solid;
		border-color:black;
	}
</style>
</head>

<body>
	<?php
		if(isset($_POST['printorder'])) { 
        	if(isset($_POST['job'])) {
				$box=$_POST['job'];
				$conStr = implode(',',$box);
				mysql_select_db($database_con1, $con1);
				$query_rs_all = "SELECT * FROM paraggelia JOIN products ON paraggelia.proionID_FK=products.proionID JOIN pelates ON paraggelia.PELATESID=pelates.PELATESID WHERE orderID in ($conStr)";
				$rs_all = mysql_query($query_rs_all, $con1) or die(mysql_error());
				$row_rs_all = mysql_fetch_assoc($rs_all);
				$totalRows_rs_all = mysql_num_rows($rs_all);	
			?>
		  <table width="900px">
				<tr>
					<td><img src="img/civillogo.png" height="60" width="150" /><td>
					<td align="right"><br /><br />Ημερομηνία Έκδοσης:&nbsp;&nbsp;<b><?php echo date("D,d/M/Y"); ?></b></td>
				</tr>
			</table>
		  
			<br />
			<br />
			<table width="900px">
				<tr>
					<td align="center" valign="bottom" height="20px" >Τύμπανα:<b><font size="+3"> <?php echo $_GET['tympano']; ?><b></font></td>
				</tr>
			
			</table>
			<table border="1" width="900px">
				<thead>
					<tr>
						<th>Όνομα</th>
						<th>Ποσότητα</th>
						<th>Σημειώσεις</th>
					</tr>
				</thead>
				<tbody>
					<?php do { ?>
						<tr class="odd gradeX" <?php if($row_rs_all['status']==2) { ?> style="text-decoration:line-through;" <?php } ?>>
						<td><?php echo $row_rs_all['PELNAME']; ?></td>
						
							<td><?php echo $row_rs_all['POSOTITA']; ?></td>
							
							<td><?php echo $row_rs_all['SHMEIWSH']; ?></td>
						</tr>
					<?php } while ($row_rs_all = mysql_fetch_assoc($rs_all)); ?>
				</tbody>
			</table>
			<div id="footer">
				<table width="900px">
					<tr>
						<td>CivilpLASTICS</td>
						<td align="center">tuv hellas<td>
						<td align="right">E.05.09/03-02-2020</td>
					</tr>
				 </table>
			</div>
      <?php }
            else {
				$printGoTo="tumpana".$_GET['tympano']."x.php";
				header(sprintf("Location: %s", $printGoTo));
			}
     } 
	elseif(isset($_POST['printmachine'])) {
		if(isset($_POST['job'])) { 
			$box=$_POST['job'];
			$conStr = implode(',',$box);
			$klise_number=$_POST['klise_number'];
			mysql_select_db($database_con1, $con1);
			$query_rs_all_kl = "SELECT * FROM paraggelia JOIN products ON paraggelia.proionID_FK=products.proionID JOIN pelates ON paraggelia.PELATESID=pelates.PELATESID JOIN printorder ON paraggelia.ORDERID=printorder.order_id_fk WHERE orderID in ($conStr) ORDER BY count_order ASC";
$rs_all_kl = mysql_query($query_rs_all_kl, $con1) or die(mysql_error());
$row_rs_all_kl = mysql_fetch_assoc($rs_all_kl);
$totalRows_rs_all_kl = mysql_num_rows($rs_all_kl);
		
		
		?>
			<table width="900px">
				<tr>
					<td><img src="img/civil trans.png" height="60" width="150" /><td>
					<td align="right"><br /><br />Ημερομηνία Έκδοσης:&nbsp;&nbsp;<b><font size="+2"><?php echo date("D,d/M/Y"); ?></b></td>
				</tr>
			</table>
		  
			<table width="900px" cellspacing="0">
            	<tr>
                	<td></td>
                	<td></td>
                	<td></td>
                	<td align="right">ΤΥΜΠΑΝΑ:<font color="red" size="+4"> <?php echo $_GET['tympano']; ?>ΑΡΙΑ</font></td>
                </tr>
            	<?php $i=1;
				do { 
				?>
           	    <tr class="top">
            	    <td><?php echo $i; ?>.</td>
            	    <td><font size="+3"><?php echo $row_rs_all_kl['PELNAME']; ?></td>
            	    <td>Κλισέ Νο=<font size="+3"><?php echo $row_rs_all_kl['klise_number_fk']; ?></font></td>
            	    <td></td>
          	    </tr>
            	  <br /><br />
            	  <tr>
            	    <td></td>
            	  <td><br /><br /><br /><br /> <font size="+2"><?php echo $row_rs_all_kl['POSOTITA']; ?>&nbsp;&nbsp;<?php echo $row_rs_all_kl['mon_metrisis']; ?></font></td>
            	    <td><br /><br />ΡΟΛΛΟΙ<font size="+1"> <textarea cols="5" rows="1"></textarea><font size="+1">ΕΚΑΤΟΣΤΑ</font></td>
            	    <td><br /><br /><font size="+2"><?php echo $row_rs_all_kl['XRWMA']; ?></font></td>
          	    </tr>
            	  <br /><br />
            	  <tr class="bottom">
            	    <td colspan="3" class="bottom"><?php echo $row_rs_all_kl['SHMEIWSH']; ?></td>
            	    <td align="right"  class="bottom"><textarea cols="15" rows="3"></textarea></td>
          	    </tr>
            <?php 
			$i++;
			 } while ($row_rs_all_kl = mysql_fetch_assoc($rs_all_kl)); ?>
			</table>
            
			<div id="footer">
				<table width="900px">
					<tr>
						<td>Civil</td>
						<td align="right">e.05.09/03-02-2020<td>
						>
					</tr>
				 </table>
			</div>          
            <?php
			 	$deleteSQL = sprintf("TRUNCATE TABLE printorder");
				mysql_select_db($database_con1, $con1);
				$Result1 = mysql_query($deleteSQL, $con1) or die(mysql_error());
            	mysql_free_result($rs_all_kl);
            ?>
		<?php }
         else {
				$printGoTo="tumpana".$_GET['tympano']."x.php";
				header(sprintf("Location: %s", $printGoTo));
			}
	}
	elseif (isset($_POST['changeExtruder'])) {
		if(isset($_POST['job'])) { 
			$box=$_POST['job'];
			$conStr = implode(',',$box);
			$updateSQL = sprintf("update paraggelia set extruder=".$_POST['extruder']." WHERE orderID in ($conStr)");
			mysql_select_db($database_con1, $con1);
			$Result1 = mysql_query($updateSQL , $con1) or die(mysql_error());	
	
			$updateGoTo="tumpana".$_GET['tympano']."x.php";
			header(sprintf("Location: %s", $updateGoTo));
		}
        else {
			$printGoTo="tumpana".$_GET['tympano']."x.php";
			header(sprintf("Location: %s", $printGoTo));
			}
	}
	?>
</body>
</html>
