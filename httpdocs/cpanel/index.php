﻿<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_con1, $con1);
$query_rs_stats = sprintf("SELECT count(*) AS pel, (SELECT count(*) FROM paraggelia) AS par, (SELECT count(*) FROM klise) AS kli, (SELECT count(*) FROM products) AS pro, (SELECT count(*) FROM users) AS sumusers FROM pelates");
$rs_stats = mysql_query($query_rs_stats, $con1) or die(mysql_error());
$row_rs_stats = mysql_fetch_assoc($rs_stats);
$totalRows_rs_stats = mysql_num_rows($rs_stats);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Civil | Dashboard</title>
    <?php include("head.php"); ?>
</head>
<body>
	<div class="container_12">
    	<?php include("header.php"); ?>
        <div class="clear"></div>
        <?php include("menu.php"); ?>
        <div class="clear"></div>
        <div class="grid_12">
        	<div class="box round first">
            	<div class="block">
                	<h3 align="center">Welcome to Civil Production Planning Program</h3>
               	</div>
         	</div>
        </div>
        <div class="clear"></div>
        <?php if($_SESSION['user_ID'] == 1) { ?>
		<div class="grid_12">
            <div class="box round">
                <h2>Στατιστικά για τον Admin</h2>
                <div class="block">
                    <div class="stat-col">
                        <span>Συνολικοί Πελάτες</span>
                        <p class="purple"><?php echo number_format( $row_rs_stats['pel'] ); ?></p>
                    </div>
                    <div class="stat-col">
                        <span>Συνολικά Προϊόντα</span>
                        <p class="yellow"><?php echo number_format( $row_rs_stats['pro'] ); ?></p>
                    </div>
                    <div class="stat-col">
                        <span>Συνολικές Παραγγελίες</span>
                        <p class="green"><?php echo number_format( $row_rs_stats['par'] ); ?></p>
                    </div>
                    <div class="stat-col">
                        <span>Συνολικά Κλισέ</span>
                        <p class="blue"><?php echo number_format( $row_rs_stats['kli'] ); ?></p>
                    </div>
                    <div class="stat-col">
                        <span>Συνολικοί Χρήστες</span>
                        <p class="red"><?php echo number_format( $row_rs_stats['sumusers'] ); ?></p>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <?php } ?>
    </div>
    <div class="clear"></div>
    <?php include("footer.php"); ?>
</body>
</html>
