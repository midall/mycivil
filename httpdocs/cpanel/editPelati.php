<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$colname_rs_pelDetails = "-1";
if (isset($_GET['pelatesid'])) {
  $colname_rs_pelDetails = $_GET['pelatesid'];
}

if ((isset($_POST["update"])) && ($_POST["update"] == "Ανανέωση")) {
	$update = sprintf("UPDATE pelates SET PELNAME = %s, cus_status = %s WHERE PELATESID = %s",
                       GetSQLValueString($_POST['PELNAME'], "text"),
					   GetSQLValueString($_POST['cus_status'], "int"),
                       GetSQLValueString($_POST['PELATESID'], "int"));
	mysql_select_db($database_con1, $con1);
  	$Result1 = mysql_query($update, $con1) or die(mysql_error());
}

mysql_select_db($database_con1, $con1);
$query_rs_pelDetails = sprintf("SELECT * FROM pelates WHERE pelates.PELATESID = %s", GetSQLValueString($colname_rs_pelDetails, "int"));
$rs_pelDetails = mysql_query($query_rs_pelDetails, $con1) or die(mysql_error());
$row_rs_pelDetails = mysql_fetch_assoc($rs_pelDetails);
$totalRows_rs_pelDetails = mysql_num_rows($rs_pelDetails);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Civil | Επεξεργασία Πελάτη </title>
    <?php include("head.php"); ?>
</head>
<body>
    <div class="container_12">
        <?php include("header.php"); ?>
        <div class="clear"></div>
      	<?php include("menu.php"); ?>
        <div class="clear"></div>
        <div class="grid_12">
            <div class="box round first grid">
                <h2>Επεξεργασία στοιχείων πελάτη: <?php echo $row_rs_pelDetails['PELNAME']; ?></h2>
                <div class="block">
					<?php if(isset($Result1) && $Result1) { ?>
                        <p style="background-color:#E6E6B8; padding:10px;">Επιτυχής Ανανέωση</p>
                    <?php } ?>
                	<form method="POST" name="form" action="<?php echo $editFormAction; ?>">
						<table>
                      		<tr>
                        		<th>ID Πελάτη(*): </th>
                            	<td><input type="text" name="PELATESID" value="<?php echo $row_rs_pelDetails['PELATESID']; ?>" required="required" readonly="readonly" /></td>
                         	</tr>
                      		<tr>
                        		<th>Όνομα Πελάτη(*): </th>
                            	<td><input type="text" name="PELNAME" value="<?php echo $row_rs_pelDetails['PELNAME']; ?>" required="required" /></td>
                         	</tr>
                      		<tr>
                        		<th>Κατηγορία Πελάτη(*): </th>
                            	<td>
                                	<select name="cus_status" id="cus_status">
                                		<option value="1" <?php if($row_rs_pelDetails['cus_status']==1) echo 'selected="selected"'; ?>>Πελάτης</option>
                                		<option value="5" <?php if($row_rs_pelDetails['cus_status']==5) echo 'selected="selected"'; ?>>Αποθήκη</option>
                                    </select>
                                </td>
                         	</tr>
                      		<tr>
                        		<th></th>
                            	<td><input type="submit" class="btn btn-red"  name="update" value="Ανανέωση" /></td>
                         	</tr>
                        </table>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"> </div>
    <?php include("footer.php"); ?>
</body>
</html>
<?php
mysql_free_result($rs_pelDetails);

mysql_free_result($rs_klise);

?>
