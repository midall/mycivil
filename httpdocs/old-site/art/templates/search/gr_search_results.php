<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//GRE" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="gre" lang="gre">
<head>
<link rel="shortcut icon" href="/images/favicon.ico" />
<meta name="robots" content="index,follow">
<META name="Author" content="e-milles creations">
<META name="Designer" content="Emilio Kyriakakis">
<meta name="copyright" content="e-milles creations Copyright 2014">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/css/english.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/responsiveslides.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/themes.css" type="text/css" media="screen" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/responsiveslides.min.js"></script>
<script src="/js/responsivemile.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/jquery.fontscale.js"></script>
<script type="text/javascript" src="/js/fontresize.js"></script>
<META name="Abstract" content="ΑΠΟΤΕΛΕΣΜΑΤΑ ΑΝΑΖΗΤΗΣΗΣ">
<META name="Description" content="ΑΠΟΤΕΛΕΣΜΑΤΑ ΑΝΑΖΗΤΗΣΗΣ">
<META name="Keywords" content="ΑΠΟΤΕΛΕΣΜΑΤΑ ΑΝΑΖΗΤΗΣΗΣ">
<META name="Title" content="ΑΠΟΤΕΛΕΣΜΑΤΑ ΑΝΑΖΗΤΗΣΗΣ">
<title>ΑΠΟΤΕΛΕΣΜΑΤΑ ΑΝΑΖΗΤΗΣΗΣ</title>
</head>
<body>

<!--header-->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top">&nbsp;</td>
<td width="1072" valign="top" align="center">
<table width="1072" border="0" cellpadding="0" cellspacing="0">
<tr>
<td height="25">&nbsp;</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50%" align="left">
<form id="search" name="search" method="get" action="/art/exec/search.cgi">
<input type="hidden" name="perpage" title="perpage" id="perpage" value="10">
<input type="hidden" name="sort_order" title="sort_order" id="sort_order" value="1,123,reverse">
<input type="hidden" name="template" title="template" id="template" value="search/gr_search_results.php">
<p><img src="/images/1x1.png" width="70" height="18" alt="left7018">
ΑΝΑΖΗΤΗΣΗ 
<input type="text" name="keyword" id="keyword" title="keyword" value="" style="border:1px #000000 solid; width:83px; height:14px;"/> 
<input type="submit" name="submit" id="submit" value="" title="Αναζήτηση" style="background:url(/images/zoom.png) no-repeat; width:22px; height:18px;"/>
</p>
</form>
</td>
<td width="50%" align="right"><a href="#"><img src="/images/en.png" width="42" height="18" alt="English" title="English"></a><a href="main.php"><img src="/images/gr.png" width="42" height="18" alt="Ελληνικά" title="Ελληνικά"></a><img src="/images/1x1.png" width="70" height="18" alt="right7018"></td>
</tr>
</table>
</td>
</tr>
<tr>
<td height="45">&nbsp;</td>
</tr>
<!--/header-->

<tr>
<td align="center">
<img src="/images/search-image.jpg" width="990" height="400" alt=""/></td>
</tr>
<tr>
<td height="70" align="center">

<!--menu-->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding:0px 5px 0px 5px;"><strong><a href="/main.php" target="_self">ΕΤΑΙΡΙΑ</a></strong></td>
<td style="padding:0px 5px 0px 5px;"><strong><a href="/products.php" target="_self">ΠΡΟΪΟΝΤΑ</a></strong></td>
<td style="padding:0px 5px 0px 5px;"><strong><a href="/clients.php" target="_self">ΠΕΛΑΤΕΣ</a></strong></td>
<td style="padding:0px 5px 0px 5px;"><strong><a href="/facilities.php" target="_self">ΕΓΚΑΤΑΣΤΑΣΕΙΣ</a></strong></td>
<td style="padding:0px 5px 0px 5px;"><strong><a href="/contact.php" target="_self">ΕΠΙΚΟΙΝΩΝΙΑ</a></strong></td>
</tr>
<tr>
<td height="6" align="center" style="padding:0px 5px 0px 5px;"><img src="/images/1x1.png" width="100%" height="6" alt="decoration"></td>
<td height="6" align="center" style="padding:0px 5px 0px 5px;"><img src="/images/1x1.png" width="100%" height="6" alt="decoration"></td>
<td height="6" align="center" style="padding:0px 5px 0px 5px;"><img src="/images/1x1.png" width="100%" height="6" alt="decoration"></td>
<td height="6" align="center" style="padding:0px 5px 0px 5px;"><img src="/images/1x1.png" width="100%" height="6" alt="decoration"></td>
<td height="6" align="center" style="padding:0px 5px 0px 5px;"><img src="/images/1x1.png" width="100%" height="6" alt="decoration"></td>
</tr>
</table>
<!--/menu-->

</td>
</tr>
</table>
</td>
<td valign="top">&nbsp;</td>
</tr>
<tr>
<td valign="top">&nbsp;</td>
<td width="1072" align="center" valign="top">
<p>&nbsp;</p>
<div align="center" style="width:700px; margin:0 auto;" class="txt">
  <!--text-->
  <p class="t4">ΑΠΟΤΕΛΕΣΜΑΤΑ ΑΝΑΖΗΤΗΣΗΣ</p>
<p>&nbsp;</p>
<p>Βρέθηκαν $search_match$ εγγραφές</p>
<p>Σελίδα $search_cpage$ από $search_tpage$</p>
<p>Αποτελέσματα Αναζήτησης απο $search_lnum$ έως $search_hnum$</p>
<hr size=2 color="#000000">
<p>&nbsp;</p>
<p>&nbsp;</p>
  <!-- template insert : $article_list$ -->
  <!-- templatecell : row -->
<p>Τιτλος: <a href="$detail_link$" target="_self">$art_name$ (Πατήστε να δείτε περισσότερα)</a></p>
<p>&nbsp;</p>
  <!-- /templatecell : row -->
    
    <!-- templatecell : row_summary -->
<p>Τιτλος: <a href="$detail_link$" target="_self">$art_name$ (Πατήστε να δείτε περισσότερα)</a></p>
<p>&nbsp;</p>
    <!-- /templatecell : row_summary -->
    
    <!-- templatecell : row_link -->
<p>Τιτλος: <a href="$detail_link$" target="_self">$art_name$ (Πατήστε να δείτε περισσότερα)</a></p>
<p>&nbsp;</p>
    <!-- /templatecell : row_link -->
    
    <!-- templatecell : row_ufile -->
<p>Τιτλος: <a href="$detail_link$" target="_self">$art_name$ (Πατήστε να δείτε περισσότερα)</a></p>
<p>&nbsp;</p>
    <!-- /templatecell : row_ufile -->
    
    <!-- templatecell : not_found -->
Λυπούμαστε, δεν βρέθηκαν Άρθρα.<br>
    <!-- /templatecell : not_found -->
    
    <!-- templatecell : img_caption -->
    <!-- /templatecell : img_caption -->
    <!-- templatecell : img_nocaption -->
    <!-- /templatecell : img_nocaption -->
    
  <div align=right>
    <!-- template insert : $prev$ -->
    <!-- templatecell : prev -->
    <a href="$prev_url$" >&lt;&lt; προηγούμενο</a>
    <!-- /templatecell : prev -->
    <!-- templatecell : no_prev -->
    <!-- /templatecell : no_prev -->
    
    <!-- template insert : $next$ -->
    <!-- templatecell : next -->
    <a href="$next_url$" >επόμενο &gt;&gt;</a>
    <!-- /templatecell : next -->
    <!-- templatecell : no_next -->
    <!-- /templatecell : no_next -->
    
    <!-- template insert : $more$ -->
    <!-- templatecell : more -->
    <!-- /templatecell : more -->
    <!-- templatecell : no_more -->
    <!-- /templatecell : no_more -->
  </div>
  <!--/text-->
</div>
</td>
<td valign="top">&nbsp;</td>
</tr>

<!--footer-->
<tr>
<td height="204" valign="bottom">&nbsp;</td>
<td width="1072" height="204" valign="bottom">
<div align="left" style="width:811px; height:204px; float:left;">
<img src="/images/civil-logo-bottom.png" alt="bottom-logo" width="811" height="204" usemap="#index-map" border="0">
<map name="index-map" id="index-map">
<area shape="rect" coords="34,56,248,183" href="/index.php" target="_self" alt="Επιστροφή στην Αρχική σελίδα" />
</map>
</div>
<div align="right" style="width:140px; height:48px; position:relative; float:right; right:30px; bottom:-150px">
<img src="/images/resize-fonts.png" width="140" height="48" usemap="#resize" border="0" alt="Αλλαγή Μεγέθους Γραμματοσειράς"><a name="fonts"></a>
<map name="resize" id="resize">
<area shape="rect" coords="0,0,45,48" href="#fonts" target="_self" id="down" title="Σμίκρυνση Γραμματοσειράς" alt="Σμίκρυνση Γραμματοσειράς">
<area shape="rect" coords="46,0,79,48" href="#fonts" target="_self" id="reset" title="Αρχικό Μέγεθος Γραμματοσειράς" alt="Αρχικό Μέγεθος Γραμματοσειράς">
<area shape="rect" coords="80,0,139,48" href="#fonts" target="_self" id="up" title="Μεγέθυνση Γραμματοσειράς" alt="Μεγέθυνση Γραμματοσειράς">
</map>
</div>
</td>
<td height="204" valign="bottom">&nbsp;</td>
</tr>
<tr>
<td height="44" valign="bottom" style="background-color:#4a4a4a;">&nbsp;</td>
<td width="1072" height="44" align="center" valign="middle" style="background-color:#4a4a4a;">
<table width="1072" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#4a4a4a;">
<tr>
<td width="463">&nbsp;</td>
<td width="146"><img src="/images/social.png" alt="Χάρτης εικόνων για την Κοινωνική Δικτύωση" width="146" height="28" usemap="#socialmap" border="0"></td>
<td width="313" align="center"><a href="/facilities.php" class="menu" title="ΕΓΚΑΤΑΣΤΑΣΕΙΣ">ΕΓΚΑΤΑΣΤΑΣΕΙΣ</a></td>
<td width="150" align="center"><a href="/contact.php" class="menu" title="ΕΠΙΚΟΙΝΩΝΙΑ">ΕΠΙΚΟΙΝΩΝΙΑ</a></td>
</tr>
</table>
<map name="socialmap" id="socialmap">
<area shape="rect" coords="24,0,59,28" href="#" target="_self" alt="Σελίδα στο FACEBOOK" title="Σελίδα στο FACEBOOK"/>
<area shape="rect" coords="77,0,117,28" href="#" target="_self" alt="Σελίδα στο TWITTER" title="Σελίδα στο TWITTER"/>
</map>
</td>
<td height="44" valign="bottom" style="background-color:#4a4a4a;">&nbsp;</td>
</tr>
</table>
<!--/footer-->

</body>
</html>