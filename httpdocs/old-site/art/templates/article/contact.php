<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//GRE" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="gre" lang="gre">
<head>
<link rel="shortcut icon" href="images/favicon.ico" />
<meta name="robots" content="index,follow">
<META name="Author" content="e-milles creations">
<META name="Designer" content="Emilio Kyriakakis">
<meta name="copyright" content="e-milles creations Copyright 2014">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/english.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/responsiveslides.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/themes.css" type="text/css" media="screen" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/responsiveslides.min.js"></script>
<script src="js/responsivemile.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.fontscale.js"></script>
<script type="text/javascript" src="js/fontresize.js"></script>
<META name="Abstract" content="$art_field1$">
<META name="Description" content="$art_field1$">
<META name="Keywords" content="$art_field2$">
<META name="Title" content="$art_name$">
<title>$art_name$</title>
<?php include_once("analyticstracking.php"); ?>
</head>
<body>
<?php include("header.php"); ?>
<tr>
<td align="center">
$art_field8$
<!-- templatecell : img_caption -->
<img src="$img_url$" alt="" border=0>
<!-- /templatecell : img_caption -->
<!-- templatecell : img_nocaption -->
<img src="$img_url$" alt="" border=0>
<!-- /templatecell : img_nocaption -->
</td>
</tr>
<tr>
<td height="70" align="center">
<?php include("menu-contact.php"); ?>
</td>
</tr>
</table>
</td>
<td valign="top">&nbsp;</td>
</tr>
<tr>
<td valign="top">&nbsp;</td>
<td width="1072" align="center" valign="top">
<p>&nbsp;</p>
<div align="center" style="width:700px; margin:0 auto;" class="txt">
$art_field3$
</div>
</td>
<td valign="top">&nbsp;</td>
</tr>
<?php include("footer.php"); ?>
</body>
</html>