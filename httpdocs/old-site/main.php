<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//GRE" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="gre" lang="gre">
<head>
<link rel="shortcut icon" href="images/favicon.ico" />
<meta name="robots" content="index,follow">
<META name="Author" content="e-milles creations">
<META name="Designer" content="Emilio Kyriakakis">
<meta name="copyright" content="e-milles creations Copyright 2014">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/english.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/responsiveslides.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/themes.css" type="text/css" media="screen" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/responsiveslides.min.js"></script>
<script src="js/responsivemile.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.fontscale.js"></script>
<script type="text/javascript" src="js/fontresize.js"></script>
<META name="Abstract" content="Η εταιρεία ΣΙΒΙΛ ΕΠΕ δραστηριοποιείται επιτυχώς από το 1981 στο χώρο των πλαστικών και χάρτου και είναι μία πρωτοπόρος βιομηχανία προϊόντων από πολυαιθυλένιο και χαρτί">
<META name="Description" content="Η εταιρεία ΣΙΒΙΛ ΕΠΕ δραστηριοποιείται επιτυχώς από το 1981 στο χώρο των πλαστικών και χάρτου και είναι μία πρωτοπόρος βιομηχανία προϊόντων από πολυαιθυλένιο και χαρτί">
<META name="Keywords" content="ΣΙΒΙΛ ΕΠΕ,πλαστικών,χάρτου,πρωτοπόρος βιομηχανία,προϊόντων από πολυαιθυλένιο,χαρτί,καινοτομίες,φρέσκες ιδέες,μηχανολογικός εξοπλισμός,πρώτες ύλες,Κεντρικής Ευρώπης,υψηλού επιπέδου ανθρώπινο δυναμικό,άριστη ποιότητα προϊόντων">
<META name="Title" content="ΕΤΑΙΡΙΑ">
<title>ΕΤΑΙΡΙΑ</title>
<?php include_once("analyticstracking.php"); ?>
</head>
<body>
<?php include("header.php"); ?>
<tr>
<td align="center">
<script>
$(function () {
$("#slider1").responsiveSlides({
maxwidth: 990,
speed: 800
});
});
</script>
<div id="wrapper">
<ul class="rslides" id="slider1">
<li><img src="/art/uploads/2-0.jpg" alt="" border=0>
<br />
<li><img src="/art/uploads/2-1.jpg" alt="" border=0>
<br />
<li><img src="/art/uploads/2-2.jpg" alt="" border=0>
<br />
<li><img src="/art/uploads/2-3.jpg" alt="" border=0>









</ul></div>
</td>
</tr>
<tr>
<td height="70" align="center">
<?php include("menu-company.php"); ?>
</td>
</tr>
</table>
</td>
<td valign="top">&nbsp;</td>
</tr>
<tr>
<td valign="top">&nbsp;</td>
<td width="1072" align="center" valign="top">
<p>&nbsp;</p>
<div align="center" style="width:700px; margin:0 auto;" class="txt">
<P>Η εταιρεία ΣΙΒΙΛ ΕΠΕ δραστηριοποιείται επιτυχώς από το 1981 στο χώρο των πλαστικών και χάρτου και είναι μία πρωτοπόρος βιομηχανία προϊόντων από πολυαιθυλένιο και χαρτί, η οποία διακρίνεται για τις καινοτομίες και τις φρέσκες ιδέες της.</P>
<P><BR>Ο άριστος μηχανολογικός εξοπλισμός σε συνδυασμό με τις άριστες πρώτες ύλες, από εταιρείες κύρους της Κεντρικής Ευρώπης, και το υψηλού επιπέδου ανθρώπινο δυναμικό, έχουν ως αποτέλεσμα την άριστη ποιότητα των προϊόντων μας και καθιστούν την εταιρεία μας ικανή να καλύψει τις ιδιαίτερες ανάγκες των πελατών μας.</P>
<P><BR>Η ΣΙΒΙΛ είναι πιστοποιημένη με TUV CERT EN ISO 9001:2008. &amp; TUV CERT EN ISO 22000.</P>
</div>
</td>
<td valign="top">&nbsp;</td>
</tr>
<?php include("footer.php"); ?>
</body>
</html>