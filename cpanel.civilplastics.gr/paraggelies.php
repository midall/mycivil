<?php
require_once( 'validation.php' );
require_once( 'Connections/con1.php' );
mysql_select_db($database_con1, $con1);


$query_rs_allparaggelies = "SELECT * ";
$query_rs_allparaggelies .= "FROM paraggelia ";
$query_rs_allparaggelies .= "INNER JOIN pelates ON paraggelia.pelatesid=pelates.pelatesid ";
$query_rs_allparaggelies .= "JOIN products ON paraggelia.proionID_FK=products.proionID ";
$query_rs_allparaggelies .= "WHERE paraggelia.status != 0 AND created_date >= NOW() - INTERVAL 3 MONTH";

$rs_allparaggelies = mysql_query($query_rs_allparaggelies, $con1) or die(mysql_error());
$row_rs_allparaggelies = mysql_fetch_assoc($rs_allparaggelies);
$totalRows_rs_allparaggelies = mysql_num_rows($rs_allparaggelies);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>Παραγγελίες | Civil</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
	<!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
	<link href="css/table/demo_page.css" rel="stylesheet" type="text/css" />
	<!-- BEGIN: load jquery -->
	<script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
	<script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
	<!-- END: load jquery -->
	<script type="text/javascript" src="js/table/table.js"></script>
	<script src="js/setup.js" type="text/javascript"></script>
	<script type="text/javascript">

		$(document).ready(function () {
			setupLeftMenu();
			
			$('.datatable').dataTable();
			setSidebarHeight();
		});
		
		function updateExt( str, str1 )
		{
			if( str == "" )
			{
				document.getElementById( "txtHint" ).innerHTML = "";
				return;
			} 
			if( window.XMLHttpRequest )
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{
				// code for IE6, IE5
				xmlhttp=new ActiveXObject( "Microsoft.XMLHTTP" );
			}
			xmlhttp.onreadystatechange=function()
			{
				if( xmlhttp.readyState == 4 && xmlhttp.status == 200 )
				{
					document.getElementById( "txtHint" ).innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open( "GET","functions/updateExtruder.php?ORDERID=" + str1 + "&extruder=" + str, true );
			xmlhttp.send();
		}
		
		function updateSak( str, str1 )
		{
			if( str == "" )
			{
				document.getElementById( "txtHint" ).innerHTML = "";
				return;
			} 
			if( window.XMLHttpRequest )
			{
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				// code for IE6, IE5
				xmlhttp = new ActiveXObject( "Microsoft.XMLHTTP" );
			}
			xmlhttp.onreadystatechange=function()
			{
				if( xmlhttp.readyState == 4 && xmlhttp.status == 200 )
				{
					document.getElementById( "txtHint" ).innerHTML = xmlhttp.responseText;
				}
			}
			xmlhttp.open( "GET", "functions/updateSak.php?ORDERID=" + str1 + "&sak=" + str, true );
			xmlhttp.send();
		}
		
		function conformbox()
		{
			alert( "Δεν είναι έτοιμη." );
			return false;
		}
	</script>
</head>
<body>
	<div class="container_12">
		<?php include( 'header.php' ); ?>
		<div class="clear"></div>				
		<?php include( 'menu.php' ); ?>
		<div class="clear"></div>
		
		<div class="grid_12">
			<div class="box round first grid">
				<div class="message success">
					<h5>Πελάτες- Εκκρεμείς Παραγγελίες</h5>
				</div>
				<div class="block">
					<div id="txtHint">
						 <!--Area for printing results -->
					</div> 
					<form name="loading" action="printLoads.php" method="post">
						<table class="data display datatable" id="example">
							<thead>
								<tr>
									<th style=" display:none">ORDERID</th>
									<th>Αρ.Παραγγ.</th>
									<th>Πελάτης</th>
									<th>Ποσοτητα</th>
									<th>Μον.Μετρ</th>
									<th>Είδος</th>
									<th>Τύπος</th>
									<th>Χρώμα</th>
									<th>Πάχος</th>
									<th>Σημειώσεις</th>
									<th>Extruder No</th>
									<th>Σακουλομηχανή<br/ >No</th>
									<th>Φόρτωση</th>
								</tr>
							</thead>
							
							<tbody>
							<?php do {							
									$db_value= $row_rs_allparaggelies['order_no'];
									$year= 2000+substr($db_value, 4, 2);
									$month= substr($db_value, 2,2);
									$day= substr($db_value, 0,2);
									$date1 = "$year-$month-$day";
									$date2 = date('Y-m-d');
									$ts1 = strtotime($date1);
									$ts2 = strtotime($date2);
									$seconds_diff = $ts2 - $ts1;
									$days=floor($seconds_diff/3600/24);
									?>
		
									<tr class="odd gradeX" <?php if($days>10) echo "bgcolor='#FF0000'"; ?>>
										<td style=" display:none"><?php echo $row_rs_allparaggelies['ORDERID']; ?></td>
										<td><?php echo $row_rs_allparaggelies['order_no']; ?></td>
										<td><a href="newParaggeliaProin.php?PELATESID=<?php echo $row_rs_allparaggelies['PELATESID']; ?>" ><?php echo $row_rs_allparaggelies['PELNAME']; ?></a></td>
										<td><?php echo $row_rs_allparaggelies['POSOTITA']; ?></td>
										<td class="center"><?php echo $row_rs_allparaggelies['mon_metrisis']; ?> </td>
										<td class="center"><?php echo $row_rs_allparaggelies['EIDOS']; ?></td>
										<td class="center"><?php echo $row_rs_allparaggelies['typos']; ?></td>
										<td><?php echo $row_rs_allparaggelies['XRWMA']; ?></td>
										<td><?php echo $row_rs_allparaggelies['PAXOS']; ?>μ</td>
										<td><b><font color="#FF0033"><?php echo $row_rs_allparaggelies['order_note']; ?></font></b></td>
										<td>
											<select name="extruder" onchange="updateExt(this.value,<?php echo $row_rs_allparaggelies['ORDERID']; ?>)">
												<option></option>
												<?php for($i=1;$i<=10;$i++) { ?>
													<option <?php if($row_rs_allparaggelies['extruder']==$i) echo "selected='selected'"; ?>><?php echo $i; ?></option>
												<?php } ?>
											</select>
										</td>
										<td>
											<select name="sak" onchange="updateSak( this.value, <?php echo $row_rs_allparaggelies['ORDERID']; ?> )">
												<option></option>
												<?php for( $i=1; $i<=15; $i++ ) { ?>
													<option <?php if($row_rs_allparaggelies['sak'] == $i) echo "selected='selected'"; ?>><?php echo $i; ?></option>
												<?php } ?>
											</select>
										</td>
										<td><input type="checkbox" name="job[]" value="<?php echo $row_rs_allparaggelies['ORDERID']; ?>" <?php if($row_rs_allparaggelies['status']==1) { ?> onclick="return conformbox();"  <?php } ?>/></td>
									 </tr>
							<?php
							} while( $row_rs_allparaggelies = mysql_fetch_assoc( $rs_allparaggelies ) ); ?>  
							</tbody>
						</table>
					</div>
					<div align="right">
						<input type="submit" value="Φόρτωση" name="changeExtruder" class="btn btn-blue">
					</div>
				</form>
			</div>
		</div>
		<div class="clear">
		</div>
	</div>
	<div class="clear"></div>
	<?php include( 'footer.php' ); ?>
</body>
</html>
<?php
mysql_free_result( $rs_allparaggelies );
?>