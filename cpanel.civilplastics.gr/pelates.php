<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_con1, $con1);
$query_rs_allPelates = "SELECT * FROM pelates WHERE cus_status=1 OR cus_status=5 ORDER BY PELNAME DESC";
$rs_allPelates = mysql_query($query_rs_allPelates, $con1) or die(mysql_error());
$row_rs_allPelates = mysql_fetch_assoc($rs_allPelates);
$totalRows_rs_allPelates = mysql_num_rows($rs_allPelates);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Civil | Πελάτες</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
			setSidebarHeight();


        });
    </script>
</head>
<body>
    <div class="container_12">
        <?php include("header.php"); ?>
     	<div class="clear"></div>
      	<?php include("menu.php"); ?>
        <div class="clear"></div>
        <div class="grid_12">
            <div class="box round first grid">
                <h2>Πελάτες</h2>
                <div class="block">
                    <table class="data display datatable" id="example">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Όνομα</th>
                                <th>Επιλογές</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php do { ?>
                            <tr class="odd gradeX">
                                <td><?php echo $row_rs_allPelates['PELATESID']; ?></td>
                                <td><a href="pelatesdetails.php?pelatesid=<?php echo $row_rs_allPelates['PELATESID']; ?>" ><?php echo $row_rs_allPelates['PELNAME']; ?></a></td>
                                <td>
                                	<a class="btn-mini btn-red btn-cross" title="Διαγραφή" href="deletePelati.php?pelatesid=<?php echo $row_rs_allPelates['PELATESID']; ?>" onclick="return confirm('Είσαι σίγουρος?');" ><span>Delete</span></a>                        
                                   <a class="btn-mini btn-black btn-check" title="Επεξεργασία" href="editPelati.php?pelatesid=<?php echo $row_rs_allPelates['PELATESID']; ?>"><span>Edit</span></a>
                                </td>
                            </tr>
                            <?php } while ($row_rs_allPelates = mysql_fetch_assoc($rs_allPelates)); ?>  
                        </tbody>
                        <tfoot>
                        </tfoot>
					</table>
            	</div>
        	</div>
		</div>
		<div class="clear"></div>
    </div>
    <div class="clear"></div>
    <?php include("footer.php"); ?>
</body>
</html>
<?php
mysql_free_result($rs_allPelates);

?>
