<?php
require_once( 'validation.php' );
require_once( 'Connections/con1.php' );

if( !function_exists( 'GetSQLValueString' ) )
{
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
	{
		if( PHP_VERSION < 6 )
		{
			$theValue = get_magic_quotes_gpc() ? stripslashes( $theValue ) : $theValue;
		}
		$theValue = function_exists('mysql_real_escape_string') ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
		
		switch( $theType )
		{
			case 'text':
				$theValue = ( $theValue != '') ? '\'' . $theValue . '\'' : 'NULL';
				break;
			case 'long':
			case 'int':
				$theValue = ($theValue != '') ? intval($theValue) : 'NULL';
				break;
			case 'double':
				$theValue = ($theValue != '') ? doubleval($theValue) : 'NULL';
				break;
			case 'date':
				$theValue = ($theValue != '') ? '\'' . $theValue . '\'' : 'NULL';
				break;
			case 'defined':
				$theValue = ($theValue != '') ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}
if( isset( $_POST['printext'] ) )
{ 
	if( isset( $_POST['job'] ) )
	{
		$box=$_POST['job'];
		$conStr = implode( ',', $box );
		
		mysql_select_db( $database_con1, $con1 );
		$query_ext = 'SELECT extruder FROM paraggelia WHERE paraggelia.orderID in (' . $conStr . ')';
		$ext = mysql_query($query_ext, $con1) or die(mysql_error());
		$row_ext = mysql_fetch_assoc($ext);
		$totalRows_ext = mysql_num_rows($ext);	
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Extruder<?php echo $row_ext['extruder'] . ' ' . date( 'd-M-Y' ); ?></title>

	<style type="text/css">
	#footer
	{
		position: absolute; bottom: 0;
	}
	@media print {
		html, body {
		height: 100%;
		margin: 0;
		padding: 0;
		}
	}
	table.order
	{
		border-collapse:collapse;
		border-bottom: 3px solid black; 
	}
	tr.order
	{
		border-bottom: 3px solid black;
	}
	td.order
	{
		padding-top:27px;
		
	}
	textarea
	{
		font-size: 20pt;
		font-family: Arial;
	} 
	</style>
</head>

<body>
<?php
if( isset( $_POST['printext'] ) )
{
	if( isset( $_POST['job'] ) )
	{
		$box=$_POST['job'];
		$conStr = implode( ',', $box );
		
		mysql_select_db($database_con1, $con1);
		$query_rs = 'SELECT * ';
		$query_rs .= 'FROM paraggelia  ';
		$query_rs .= 'JOIN products ON paraggelia.proionID_FK = products.proionID  ';
		$query_rs .= 'JOIN pelates ON paraggelia.PELATESID = pelates.PELATESID  ';
		$query_rs .= 'JOIN entoles_ext ON products.proionID = entoles_ext.product_id_fk  ';
		$query_rs .= 'LEFT JOIN printorder ON paraggelia.ORDERID = printorder.order_id_fk  ';
		$query_rs .= 'WHERE paraggelia.orderID IN ( ' . $conStr . ') AND paraggelia.status <> 0  ';
		$query_rs .= 'ORDER BY count_order ASC';
		$rs = mysql_query( $query_rs, $con1 ) or die( mysql_error() );
		$row_rs = mysql_fetch_assoc($rs);
		$totalRows_rs = mysql_num_rows($rs);	
?>
		<table width="900px">
			<tr>
				<td><img src="img/civillogo.png" height="60" width="150" /><td>
				<td align="right"><br /><br />Ημερομηνία Έκδοσης:&nbsp;&nbsp;<b><?php echo date("D,d/M/Y"); ?></b></td>
			</tr>
		</table>
		
		<table width="900px">
			<tr>
				<td align="center" valign="bottom" height="20px" >	Extruder:<b><font size="+3"> <?php echo $row_rs['extruder']; ?><b></font></td>
			</tr>
		</table>
		<table width="900px" class="order">
			<?php 
			$i=1;
			do { ?>
			<tr class="top">
				<td class="order" style="border-right:#000 solid thin; border-bottom:#000 solid thin;"><b><font size="+1"><?php echo $i.") ".$row_rs['POSOTITA']; ?>&nbsp;κιλά&nbsp; <?php echo $row_rs['PlastType']; ?><br /><?php echo $row_rs['EIDOS']; ?>, <?php echo $row_rs['typos']; ?></b></font></td>
				<td style="border-right:#000 solid thin; border-bottom:#000 solid thin;"><font size="+1"><strong>Χρώμα: </strong><?php echo $row_rs['XRWMA']; ?></font><br /></td>
				<td colspan="2" style="border-bottom:#000 solid thin;"><font size="+1"><strong>Πάχος: </strong><?php echo $row_rs['PAXOS']; ?> &nbsp;&nbsp;&nbsp;<strong>Τύμπανο:</strong></font><?php echo $row_rs['product_tympano']; ?><br /></td>
			</tr>
			<tr>
				<td class="order" style="border-right:#000 solid thin; border-bottom:#000 solid thin;"><font size="+1"><strong>Υλικό:</strong><?php echo $row_rs['PlastMaterial']; ?>&nbsp;<?php echo $row_rs['PlastInfo']; ?></font></td>
				<td class="order" style="border-right:#000 solid thin; border-bottom:#000 solid thin;" colspan="2"><font size="+1"><strong>Συσκευασία:</strong><?php echo $row_rs['Plast_Syskevasia']; ?></font></td>
				<td class="order" style="border-bottom:#000 solid thin;"> <strong>Βάρος Δέματος:</strong><?php echo $row_rs['Weight']; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="order" bgcolor="#CCFFFF" style="border-bottom:#000 solid thin; border-right:#000 solid thin;"><font size="+2"><?php echo $row_rs['Info_ext']; ?></font></td>
				<td class="order" style="border-bottom:#000 solid thin; border-right:#000 solid thin;"><font size="+1"><strong>Εκτύπωση:</strong></font><?php if($row_rs['product_printed']==1) echo "NAI"; elseif($row_rs_pelDetails['product_printed']==0) echo "OXI"; ?><br /></td>
				<td class="order" style="border-bottom:#000 solid thin;"><b><font size="+2"><strong>Πελάτης:</strong><?php echo $row_rs['PELNAME']; ?></font></b></td>
			</tr>
			<tr>
				<td colspan="4" style="border-bottom:#000 solid thin; text-align: center;"><strong>ΕΚΤΕΛΕΣΗ ΣΥΝΤΑΓΗΣ - ΙΧΝΗΛΑΣΙΜΟΤΗΤΑ</strong></td>
			</tr>
			<tr>
				<td colspan="2" style="border-bottom:#000 solid thin;"><strong>ΗΜΕΡΟΜΗΝΙΑ ΠΑΡΑΓΩΓΗΣ:</strong></td>
				<td colspan="2" style="border-bottom:#000 solid thin;"><strong>ΥΛΙΚΑ:</strong></td>
			</tr>
			<tr>
				<td style="border-bottom:#000 solid thin;"><font size="+1">1.</font></td>
				<td style="border-bottom:#000 solid thin;"><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.</font></td>
				<td colspan="2" style="border-bottom:#000 solid thin;"><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.</font></td>
			</tr>
			<tr>
				<td style="border-bottom:#000 solid thin;"><font size="+1">4.</font></td>
				<td style="border-bottom:#000 solid thin;"><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.</font></td>
				<td colspan="2" style="border-bottom:#000 solid thin;"><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.</font></td>
			</tr>
			<tr class="order">
				<td style="border-bottom:#000 solid thin;"><font size="+1">7.</font></td>
				<td style="border-bottom:#000 solid thin;"><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8.</font></td>
				<td colspan="2" style="border-bottom:#000 solid thin;"><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9.</font></td>
			</tr>
			<?php
			$i++;
			}
			while( $row_rs = mysql_fetch_assoc( $rs ) ); ?>
		</table>
		
		<div id="footer">
			<table width="900px">
				<tr>
					<td>Civil Plastics</td>
					<td align="center">E.05.05|03/02/2020<td>
					<td align="right">TUV HELLAS</td>
				</tr>
			</table>
		</div>
		<?php
		$deleteSQL = sprintf("TRUNCATE TABLE printorder");
		mysql_select_db($database_con1, $con1);
		$Result1 = mysql_query($deleteSQL, $con1) or die(mysql_error());
		mysql_free_result($rs_all_kl);
		}
		else
		{
			$printGoTo = 'tumpana' . $_GET['tympano'] . 'x.php';
			header( sprintf( 'Location: %s', $printGoTo ) );
		}
	}		
?>
</body>
</html>
<?php
mysql_free_result( $rs );
?>
