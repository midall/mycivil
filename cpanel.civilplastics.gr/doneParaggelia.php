<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
mysql_select_db($database_con1, $con1);
$query_rs_allparaggelies = "SELECT * FROM paraggelia INNER JOIN pelates ON paraggelia.pelatesid=pelates.pelatesid JOIN products  ON paraggelia.proionID_FK=products.proionID WHERE status=0";
$rs_allparaggelies = mysql_query($query_rs_allparaggelies, $con1) or die(mysql_error());
$row_rs_allparaggelies = mysql_fetch_assoc($rs_allparaggelies);
$totalRows_rs_allparaggelies = mysql_num_rows($rs_allparaggelies);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Civil | Admin Panel</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
			setSidebarHeight();


        });
    </script>
</head>
<body>
    <div class="container_12">
        <?php include("header.php"); ?>
        
        <div class="clear">
        </div>
      <?php include("menu.php"); ?>
        <div class="clear">
        </div>
        <div class="grid_12">
            <div class="box round first grid">
            <div class="message warning">
                <h5>
                    Πελάτες- Εκτελέσμενες Παραγγελίες</h5>
                    </div>
                <div class="block">
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
                        	<th style="display:none">ORDERID</th>
							<th>Αρ.Παραγγ.</th>
						    <th>Πελάτης</th>
							<th>Ποσοτητα</th>
							<th>Μον.Μετρ</th>
							<th>Είδος</th>
                            <th>Τύπος</th>
                            <th>Χρώμα</th>
                            <th>Πάχος</th>
                            <th>Σημειώσεις</th>
                            
						</tr>
					</thead>
                    
					<tbody>
                    
						<?php do { ?>
					    <tr class="odd gradeX">
                        	<td style="display:none"><?php echo $row_rs_allparaggelies['ORDERID']; ?></td>
						    <td><?php echo $row_rs_allparaggelies['order_no']; ?></td>
						    <td><?php echo $row_rs_allparaggelies['PELNAME']; ?></td>
						    <td><?php echo $row_rs_allparaggelies['POSOTITA']; ?></td>
						    <td class="center"><?php echo $row_rs_allparaggelies['mon_metrisis']; ?> </td>
						    <td class="center"><?php echo $row_rs_allparaggelies['EIDOS']; ?></td>
                             <td class="center"><?php echo $row_rs_allparaggelies['typos']; ?></td>
                            <td><?php echo $row_rs_allparaggelies['XRWMA']; ?></td>
                            <td><?php echo $row_rs_allparaggelies['PAXOS']; ?>μ</td>
                            <td><?php echo $row_rs_allparaggelies['SHMEIWSH']; ?></td>
					      </tr>
						<?php } while ($row_rs_allparaggelies = mysql_fetch_assoc($rs_allparaggelies)); ?>  
                    </tbody>
				</table>
                    
                </div>
            </div>
        </div>
      <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("footer.php"); ?>
</body>
</html>
<?php
mysql_free_result($rs_allparaggelies);

?>
