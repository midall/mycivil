<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form")) {
  $updateSQL = sprintf("UPDATE paraggelia SET POSOTITA=%s, proionID_FK=%s WHERE ORDERID=%s",
                       GetSQLValueString($_POST['POSOTITA'], "text"),
                       GetSQLValueString($_POST['proionID_FK'], "int"),
                       GetSQLValueString($_POST['ORDERID'], "int"));

  mysql_select_db($database_con1, $con1);
  $Result1 = mysql_query($updateSQL, $con1) or die(mysql_error());

  $updateGoTo = "newParaggeliaProin.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rs_EditParag = "-1";
if (isset($_GET['ORDERID'])) {
  $colname_rs_EditParag = $_GET['ORDERID'];
}
mysql_select_db($database_con1, $con1);
$query_rs_EditParag = sprintf("SELECT * FROM paraggelia JOIN pelates on paraggelia.PELATESID=pelates.PELATESID WHERE ORDERID = %s", GetSQLValueString($colname_rs_EditParag, "int"));
$rs_EditParag = mysql_query($query_rs_EditParag, $con1) or die(mysql_error());
$row_rs_EditParag = mysql_fetch_assoc($rs_EditParag);
$totalRows_rs_EditParag = mysql_num_rows($rs_EditParag);

$colname_rs_allproionta = "-1";
if (isset($_GET['PELATESID'])) {
  $colname_rs_allproionta = $_GET['PELATESID'];
}
mysql_select_db($database_con1, $con1);
$query_rs_allproionta = sprintf("SELECT * FROM products WHERE PELATESID = %s", GetSQLValueString($colname_rs_allproionta, "int"));
$rs_allproionta = mysql_query($query_rs_allproionta, $con1) or die(mysql_error());
$row_rs_allproionta = mysql_fetch_assoc($rs_allproionta);
$totalRows_rs_allproionta = mysql_num_rows($rs_allproionta);
?>

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Typography | BlueWhale Admin</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
			setSidebarHeight();


        });
    </script>
</head>
<body>
    <div class="container_12">
        <?php include("header.php"); ?>
        
        <div class="clear">
        </div>
      <?php include("menu.php"); ?>
        <div class="clear">
        </div>
        <?php include("sidemenu.php"); ?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>
              Επεξεργασία Παραγγελίας:</h2>
                <div class="block">
                    
                    <b>Πελάτης:</b> <?php echo $row_rs_EditParag['PELNAME']; ?><br /><br />
                    

               
                  <form method="POST" name="form" action="<?php echo $editFormAction; ?>">
<table width="200" border="1">
 <tr>
    <th scope="row">  </th>
    <td><input name="ORDERID" type="text" value="<?php echo $row_rs_EditParag['ORDERID']; ?>" hidden="hidden" /></td>
  </tr>
 <tr>
    <th scope="row">Ποσότητα  </th>
    <td><input name="POSOTITA" type="text" value="<?php echo $row_rs_EditParag['POSOTITA']; ?>" /></td>
  

  </tr>
   <tr>
    <th scope="row"></th>
    <td><select name="proionID_FK">
                            		  <?php do { ?>
                            		    <option value="<?php echo $row_rs_allproionta['proionID']; ?>"><?php echo $row_rs_allproionta['EIDOS']; ?>&nbsp;<?php echo $row_rs_allproionta['typos']; ?>&nbsp;<?php echo $row_rs_allproionta['PAXOS']; ?>μ</option>
                            		  <?php } while ($row_rs_allproionta = mysql_fetch_assoc($rs_allproionta)); ?>
                           		     </select></td>
  

  </tr>
 
  
</table>

                    <input class="btn btn-red"  name="" type="submit" />
                   
                    <input type="hidden" name="MM_update" value="form" />
                  </form>
                </div>
            </div>
        </div>
      <div class="clear">
      </div>
    </div>
    <div class="clear">
    </div>
    <?php include("footer.php"); ?>
</body>
</html>
<?php
mysql_free_result($rs_EditParag);

mysql_free_result($rs_allproionta);

?>
