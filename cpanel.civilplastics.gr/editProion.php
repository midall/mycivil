<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form"))
{
	if( isset( $_FILES ) && isset( $_FILES['specification_document'] ) && $_FILES['specification_document']['tmp_name'] != '' )
	{
		$target_file = time() . '_' . $_FILES['specification_document']['name'];
		$target_path = 'documents/' . $target_file;
		
		move_uploaded_file( $_FILES['specification_document']['tmp_name'], $target_path );
		
		// Documents
		$insertSQL = sprintf( "INSERT INTO documents ( document_name, document_server_name ) VALUES ( %s, %s )",
                       GetSQLValueString( $_FILES['specification_document']['name'], "text" ),
					   GetSQLValueString( $target_file, "text" ) );
		
		mysql_select_db($database_con1, $con1);
		$Result1 = mysql_query($insertSQL, $con1) or die(mysql_error());
		$specification_document_id = mysql_insert_id();
	}
	else
	{
		$specification_document_id = $_POST['specification_document_id_existing'] == '' ? NULL : $_POST['specification_document_id_existing'];
	}
	
  $updateSQL = sprintf("UPDATE products SET EIDOS=%s, typos=%s, XRWMA=%s, PAXOS=%s, SYSKEVASIA=%s, SHMEIWSH=%s, mon_metrisis=%s, imerominia=curdate(), product_tympano=%s, product_code=%s, specification_document_id = %s WHERE proionID=%s",
                       GetSQLValueString($_POST['eidos'], "text"),
					   GetSQLValueString($_POST['typos'], "text"),
                       GetSQLValueString($_POST['xrwma'], "text"),
                       GetSQLValueString($_POST['paxos'], "text"),
                       GetSQLValueString($_POST['syskevasia'], "text"),
                       GetSQLValueString($_POST['shmeiwsh'], "text"),
					   GetSQLValueString($_POST['mon_metrisis'], "text"),
					   GetSQLValueString($_POST['product_tympano'], "text"),
					   GetSQLValueString($_POST['product_code'], "text"),
					   GetSQLValueString($specification_document_id, "int"),
                       GetSQLValueString($_POST['proionID'], "int"));

  mysql_select_db($database_con1, $con1);
  $Result1 = mysql_query($updateSQL, $con1) or die(mysql_error());
  
  $updateSQL = sprintf("UPDATE entoles_ext SET PlastType=%s, PlastMaterial=%s, PlastInfo=%s, Weight=%s, Plast_Syskevasia=%s, Info_ext=%s, product_printed=%s WHERE product_id_fk=%s",
                       GetSQLValueString($_POST['PlastType'], "text"),
					   GetSQLValueString($_POST['PlastMaterial'], "text"),
                       GetSQLValueString($_POST['PlastInfo'], "text"),
                       GetSQLValueString($_POST['Weight'], "text"),
                       GetSQLValueString($_POST['Plast_Syskevasia'], "text"),
                       GetSQLValueString($_POST['Info_ext'], "text"),
                       GetSQLValueString($_POST['product_printed'], "int"),
                       GetSQLValueString($_POST['proionID'], "int"));

  mysql_select_db($database_con1, $con1);
  $Result1 = mysql_query($updateSQL, $con1) or die(mysql_error());

  $updateGoTo = "pelatesdetails.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}
$colname_rs_ProionEdit = "-1";
if (isset($_GET['proionID'])) {
  $colname_rs_ProionEdit = $_GET['proionID'];
}
mysql_select_db($database_con1, $con1);
$query_rs_ProionEdit = sprintf("SELECT * FROM products JOIN  pelates ON products.pelatesID=pelates.pelatesID LEFT JOIN entoles_ext ON products.proionID=entoles_ext.product_id_fk WHERE products.proionID=%s", GetSQLValueString($colname_rs_ProionEdit, "int"));
$rs_ProionEdit = mysql_query($query_rs_ProionEdit, $con1) or die(mysql_error());
$row_rs_ProionEdit = mysql_fetch_assoc($rs_ProionEdit);
$totalRows_rs_ProionEdit = mysql_num_rows($rs_ProionEdit);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Civil | Επεξεργασία Προϊόντος</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
			setSidebarHeight();


        });
    </script>
</head>
<body>
    <div class="container_12">
        <?php include("header.php"); ?>
        
        <div class="clear">
        </div>
      <?php include("menu.php"); ?>
        <div class="clear">
        </div>
        
        <div class="grid_12">
            <div class="box round first grid">
                <h2>
              Επεξεργασία Προϊόντος:</h2>
                <div class="block">
                    
                    <b>Πελάτης:</b> <?php echo $row_rs_ProionEdit['PELNAME']; ?><br /><br />
                    

               
                  <form method="POST" name="form" action="<?php echo $editFormAction; ?>" enctype="multipart/form-data">
                        <table border="1">
                         <tr>
                            <th scope="row" width="300px">  </th>
                            <td><input name="proionID" type="text" value="<?php echo $row_rs_ProionEdit['proionID']; ?>" hidden="hidden" /></td>
                          </tr>
                         <tr>
                            <th scope="row">Είδος  </th>
                            <td><input name="eidos" type="text" value="<?php echo $row_rs_ProionEdit['EIDOS']; ?>" /></td>
                          
                        
                          </tr>
                           <tr>
                            <th scope="row">Τύπος  </th>
                            <td>                            
                            	<select name="typos" id="typos">
                                    <option value="ΤΣΑΝΤΑ" <?php if($row_rs_ProionEdit['typos']=='ΤΣΑΝΤΑ') echo 'selected="selected"'; ?>>ΤΣΑΝΤΑ</option>
                                    <option value="ΦΥΛΛΟ" <?php if($row_rs_ProionEdit['typos']=='ΦΥΛΛΟ') echo 'selected="selected"'; ?>>ΦΥΛΛΟ</option>
                                    <option value="ΣΑΚΟΥΛΑΚΙ ΔΙΑΦΑΝΕΣ" <?php if($row_rs_ProionEdit['typos']=='ΣΑΚΟΥΛΑΚΙ ΔΙΑΦΑΝΕΣ') echo 'selected="selected"'; ?>>ΣΑΚΟΥΛΑΚΙ ΔΙΑΦΑΝΕΣ</option>
                                    <option value="ΣΩΛΗΝΑΣ" <?php if($row_rs_ProionEdit['typos']=='ΣΩΛΗΝΑΣ') echo 'selected="selected"'; ?>>ΣΩΛΗΝΑΣ</option>
                                    <option value="ΣΑΚΟΣ" <?php if($row_rs_ProionEdit['typos']=='ΣΑΚΟΣ') echo 'selected="selected"'; ?>>ΣΑΚΟΣ</option>
                                </select>
                            </td>
                          
                        
                          </tr>
                          <tr>
                            <th scope="row">Χρώμα</th>
                            <td><input name="xrwma" type="text" class="error" value="<?php echo $row_rs_ProionEdit['XRWMA']; ?>" /></td>
                          </tr>
                          <tr>
                            <th scope="row">Πάχος;</th>
                            <td><input name="paxos" type="text" class="success" value="<?php echo $row_rs_ProionEdit['PAXOS']; ?>" /></td>
                          </tr>
                          <!--OUT BY GINA
                          <tr>
                            <th scope="row">Συσκευασία</th>
                            <td><input name="syskevasia" type="text" value="<?php echo $row_rs_ProionEdit['SYSKEVASIA']; ?>" /></td>
                          </tr>
                           <tr>
                            <th scope="row">Μον. Μέτρησης </th>
                            <td><input name="mon_metrisis" type="text" value="<?php echo $row_rs_ProionEdit['mon_metrisis']; ?>" /></td>
                          </tr>
                          -->
                          <tr>
                            <th scope="row">Λεπτομέρειες</th>
                            <td><textarea name="shmeiwsh" cols="40" rows="5"><?php echo $row_rs_ProionEdit['SHMEIWSH']; ?></textarea></td>
                          </tr>
                          <tr>
                                <th scope="row">Μέγεθος Τυμπάνου</th>
                                <td>
                                    <select name="product_tympano" required="required" >
                                        <option></option>
                                        <option value="0" <?php if($row_rs_ProionEdit['product_tympano']=='0') echo 'selected="selected"'; ?>>Ατύπωτο</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='30x') echo 'selected="selected"'; ?>>30x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='35x') echo 'selected="selected"'; ?>>35x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='40x') echo 'selected="selected"'; ?>>40x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='45x') echo 'selected="selected"'; ?>>45x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='50x') echo 'selected="selected"'; ?>>50x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='55x') echo 'selected="selected"'; ?>>55x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='60x') echo 'selected="selected"'; ?>>60x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='65x') echo 'selected="selected"'; ?>>65x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='70x') echo 'selected="selected"'; ?>>70x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='75x') echo 'selected="selected"'; ?>>75x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='80x') echo 'selected="selected"'; ?>>80x</option>
                                        <option <?php if($row_rs_ProionEdit['product_tympano']=='85x') echo 'selected="selected"'; ?>>85x</option>
                                    </select>
                                </td>
                    	  </tr>
                          <tr>
                            <th scope="row">Κωδικός Προιόντος:</th>
                            <td><input name="product_code" type="text" class="success" value="<?php echo $row_rs_ProionEdit['product_code']; ?>" /></td>
                          </tr>
                          <tr>
							<?php
							// Specification
							$specification_document = '';
							if( $row_rs_ProionEdit['specification_document_id'] != NULL )
							{
								$query_rs_specDocument = sprintf("SELECT * FROM documents WHERE document_id = %s", GetSQLValueString( $row_rs_ProionEdit['specification_document_id'], "int"));
								$rs_specDocument = mysql_query( $query_rs_specDocument, $con1 ) or die(mysql_error());
								$row_rs_specDocument = mysql_fetch_assoc($rs_specDocument);

								$specification_document = '<a href="documents/' . $row_rs_specDocument['document_server_name'] . '" download="' . $row_rs_specDocument['document_name'] . '" target="_blank">Download</a>';
							}
							?>
							<input name="specification_document_id_existing" type="hidden" value="<?php echo $row_rs_ProionEdit['specification_document_id']; ?>" />
                            <th scope="row">Specification Sheet:</th>
                            <td><?php echo $specification_document; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Specification Sheet New:</th>
                            <td><input name="specification_document" type="file" class="success" /></td>
                          </tr>
                          <tr>
                                <th scope="row" colspan="2" bgcolor="#CCCCCC" valign="middle" height="50px" ><br />Στοιχεία Extruder</th>
                              </tr>
                          <tr>
                                <th scope="row">Extruder</th>
                                <td><input name="PlastType" type="text" value="<?php echo $row_rs_ProionEdit['PlastType']; ?>" /></td>
                              </tr>
                              <tr>
                                <th scope="row">Υλικό </th>
                                <td><input name="PlastMaterial" type="text" value="<?php echo $row_rs_ProionEdit['PlastMaterial']; ?>" /></td>
                              </tr>
                              <tr>
                                <th scope="row">Συνταγή</th>
                                <td><input name="PlastInfo" type="text" class="error" value="<?php echo $row_rs_ProionEdit['PlastInfo']; ?>" /></td>
                              </tr>
                              <tr>
                                <th scope="row">Συσκευασία</th>
                                <td><input name="Plast_Syskevasia" type="text" value="<?php echo $row_rs_ProionEdit['Plast_Syskevasia']; ?>" /></td>
                              </tr>
                               <tr>
                                <th scope="row">Βαρος Δεματος</th>
                                <td><input name="Weight" type="text" class="success" value="<?php echo $row_rs_ProionEdit['Weight']; ?>" /></td>
                              </tr>
                               <tr>
                                <th scope="row">Έκτυπωμένο </th>
                                <td> ΝΑΙ<input name="product_printed" type="radio" class="success" value="1" <?php if($row_rs_ProionEdit['product_printed']==1) { echo 'checked="checked"'; } ?>/> ΌΧΙ<input name="product_printed" type="radio" class="success" value="0" <?php if($row_rs_ProionEdit['product_printed']==0) { echo 'checked="checked"'; } ?> /></td>
                              </tr>
                              <tr>
                                <th scope="row">Λεπτομέρειες</th>
                                <td><textarea name="Info_ext" cols="40" rows="5"><?php echo $row_rs_ProionEdit['Info_ext']; ?></textarea></td>
                              </tr>
  
</table>

                    <input class="btn btn-red"  name="" type="submit" />
                    
                    <input type="hidden" name="MM_insert" value="form" />
                    <input type="hidden" name="MM_update" value="form" />
                  </form>
                </div>
            </div>
        </div>
      <div class="clear">
      </div>
    </div>
    <div class="clear">
    </div>
    <?php include("footer.php"); ?>
</body>
</html>
<?php
mysql_free_result($rs_ProionEdit);

?>
