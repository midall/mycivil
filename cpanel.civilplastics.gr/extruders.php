<?php require_once('Connections/con1.php'); ?>
<?php require_once('validation.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
	$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
	case "text":
	  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
	  break;	
	case "long":
	case "int":
	  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
	  break;
	case "double":
	  $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
	  break;
	case "date":
	  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
	  break;
	case "defined":
	  $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
	  break;
  }
  return $theValue;
}
}

$colname_rs_ext = "-1";
if (isset($_GET['ext'])) {
  $colname_rs_ext = $_GET['ext'];
}
mysql_select_db($database_con1, $con1);

$query = "SELECT * FROM paraggelia ";
$query .= "JOIN products ON paraggelia.proionID_FK=products.proionID ";
$query .= "JOIN pelates ON paraggelia.PELATESID=pelates.PELATESID ";
$query .= "JOIN entoles_ext ON products.proionID=entoles_ext.product_id_fk ";
$query .= "WHERE extruder = %s AND paraggelia.status<>0 ";
$query .= "ORDER BY paraggelia.ORDERID ";

$query_rs_ext = sprintf( $query, GetSQLValueString($colname_rs_ext, "int"));
$rs_ext = mysql_query($query_rs_ext, $con1) or die(mysql_error());
$row_rs_ext = mysql_fetch_assoc($rs_ext);
$totalRows_rs_ext = mysql_num_rows($rs_ext);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Civil | Extruders</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
	<!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
	<link href="css/table/demo_page.css" rel="stylesheet" type="text/css" />
	<!-- BEGIN: load jquery -->
	<script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
	<script src="js/table/jquery.dataTables.min.js" type="text/javascript"></script>
	<!-- END: load jquery -->
	<script type="text/javascript" src="js/table/table.js"></script>
	<script src="js/setup.js" type="text/javascript"></script>
	<script type="text/javascript">

		$(document).ready(function () {
			setupLeftMenu();

			$('.datatable').dataTable();
			setSidebarHeight();


		});
		function checkAll(bx) {
  			var cbs = document.getElementsByTagName('input');
 			for(var i=0; i < cbs.length; i++) {
			if(cbs[i].type == 'checkbox') {
  				cbs[i].checked = bx.checked;
			}
		  }
		}
	</script>
	<script>
	function showUser1(str2)
	{
		if (str2=="")
		  {
		  document.getElementById("txtHint").innerHTML="";
		  return;
		  } 
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
			xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
			document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET","functions/putOrder.php?ORDERID="+str2,true);
		xmlhttp.send();
	}
	</script>
</head>
<body>
	<div class="container_12">
		<?php include( 'eader.php' ); ?>
		
		<div class="clear"></div>
		<?php include( 'menu.php' ); ?>
		<div class="clear"></div>

		<div class="grid_12">
			<div class="box round first grid">
				<h2>Extruder <?php echo $_GET['ext']; ?></h2>
				<div id="txtHint">
					 <!--Area for printing results -->
				</div> 
				<div class="block">
					<form action="printExtruder.php" method="post" >
						<table class="data display datatable" id="example">
							<thead>
								<tr>
									<th style="display: none;"></th>
									<th><input type="checkbox" onclick="checkAll(this)"></th>
									<th>Αρ.Παραγγ.</th>
								  	<th>Πελάτης</th>
									<th>Ποσότητα</th>
									<th>Μον. Μέτρησης</th>
									<th>Είδος</th>
									<th>Χρώμα</th>
									<th>Πάχος</th>
									<th>Σημειώσεις</th>
									<td>Κατάσταση</td>
									
								</tr>
							</thead>
							<tbody>
								<?php do { ?>
								<tr class="odd gradeX" <?php if($row_rs_ext['status']==3 || $row_rs_ext['status']==2) { ?> style="text-decoration:line-through;" <?php } ?>>
									<td style="display: none;"><?php echo $row_rs_ext['ORDERID']; ?></td>
								  	<td><input type="checkbox" name="job[]" value="<?php echo $row_rs_ext['ORDERID']; ?>" onClick="showUser1(this.value)" /></td>
									<td><?php echo $row_rs_ext['order_no']; ?></td>
									<td><?php echo $row_rs_ext['PELNAME']; ?></td>
									<td><?php echo $row_rs_ext['POSOTITA']; ?></td>
									<td><?php echo $row_rs_ext['mon_metrisis']; ?></td>
									<td><?php echo $row_rs_ext['PlastType']; ?></td>
									<td><?php echo $row_rs_ext['XRWMA']; ?></td>
									<td><?php echo $row_rs_ext['PAXOS']; ?></td>
									<td><?php echo $row_rs_ext['Info_ext']; ?></td>
									<td><?php if($row_rs_ext['status']==1) { ?><a class="btn btn-pink" href="updateExtStatus.php?ORDERID=<?php echo $row_rs_ext['ORDERID']; ?>&ext=<?php echo $_GET['ext']; ?>"><span></span>OK</a><?php } ?></td>
									</tr>
								<?php } while ($row_rs_ext = mysql_fetch_assoc($rs_ext)); ?>
							</tbody>
						</table>
						<div class="clear"></div>
						<input type="submit" value="Πρόγραμμα Extruder" name="printext" class="btn btn-purple">
					</form>
				</div>
			</div>
		</div>
	  <div class="clear">
		</div>
	</div>
	<div class="clear">
	</div>
	<?php include("footer.php"); ?>
</body>
</html>
<?php
mysql_free_result($rs_ext);

mysql_free_result($rs_entoles_ext);

mysql_free_result($rs_allparaggelies);

?>
