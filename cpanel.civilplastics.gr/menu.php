<div class="grid_12">
	<ul class="nav main">
		<li class="ic-dashboard"><a href="index.php"><span>Αρχική</span></a> </li>
		<li class="ic-dashboard"><a href="#"><span>Πελάτες</span></a>
			<ul>
				<li><a href="newPelates.php">Νέος Πελάτης</a> </li>
				<li><a href="pelates.php">Καρτέλα (Προιόντα) Πελάτη</a> </li>
				<li><a href="kartelaPelath.php">Παραγγελίες Πελάτη</a> </li>
			</ul>
		</li>
		<li class="ic-dashboard"><a href="paraggelies.php"><span>Παραγγελίες</span></a>
			<ul>
				<li><a href="todayParaggelia.php">Σημερινές Παραγγελίες</a> </li>
				<li><a href="doneParaggelia.php">Εκτελεσμένες Παραγγελίες</a> </li>
				 <li><a href="kartelaPelath.php">Νέα Παραγγελία</a> </li>
			</ul>
		</li>
		<li class="ic-dashboard"><a href="#"><span>Τύμπανα</span></a>
			<ul>
				<li><a href="tympana.php?tympano=30x">30x</a> </li>
				<li><a href="tympana.php?tympano=35x">35x</a> </li>
				<li><a href="tympana.php?tympano=40x">40x</a> </li>
				<li><a href="tympana.php?tympano=45x">45x</a> </li>
				<li><a href="tympana.php?tympano=50x">50x</a> </li>
				<li><a href="tympana.php?tympano=55x">55x</a> </li>
				<li><a href="tympana.php?tympano=60x">60x</a> </li>
				<li><a href="tympana.php?tympano=65x">65x</a> </li>
				<li><a href="tympana.php?tympano=70x">70x</a> </li>
				<li><a href="tympana.php?tympano=75x">75x</a> </li>
				<li><a href="tympana.php?tympano=80x">80x</a> </li>
				<li><a href="tympana.php?tympano=85x">85x</a> </li>
				<li><a href="ypoloipaEidi.php?tympano=0">Υπόλοιπα Είδη</a> </li>
			</ul>
		</li>
		<li class="ic-dashboard"><a href="#"><span>Extruders</span></a>
			<ul>
				<li><a href="extruders.php?ext=1">1</a> </li>
				<li><a href="extruders.php?ext=2">2</a> </li>
				<li><a href="extruders.php?ext=3">3</a> </li>
				<li><a href="extruders.php?ext=4">4</a> </li>
				<li><a href="extruders.php?ext=5">5</a> </li>
				<li><a href="extruders.php?ext=6">6</a> </li>
				<li><a href="extruders.php?ext=7">7</a> </li>
				<li><a href="extruders.php?ext=8">8</a> </li>
				<li><a href="extruders.php?ext=9">9</a> </li>
				<li><a href="extruders.php?ext=10">10</a> </li>
			</ul>
		</li>
		<li class="ic-dashboard"><a href="#"><span>Σακουλομηχανές</span></a>
			<ul>
				<?php
				for( $i = 1; $i <= 15; $i++ )
				{
					echo '<li><a href="sakoulomixani.php?sak=' . $i . '">' . $i . '</a> </li>' . PHP_EOL;
				}
				?>
			</ul>
		</li>
			 
		<!--
		<li class="ic-form-style"><a href="typography.php"><span>Typografy</span></a>
			<ul>
				<li><a href="form-controls.php">Forms</a> </li>
				<li><a href="buttons.php">Buttons</a> </li>
				<li><a href="form-controls.php">Full Page Example</a> </li>
				<li><a href="charts.php">Charts & Graphs</a> </li>
				<li><a href="table.php">Data Table</a> </li>
				<li><a href="table.php">Page with Sidebar Example</a> </li>
				<li><a href="gallery-with-filter.php">Gallery with Filter</a> </li>
				<li><a href="image-gallery.php">Pretty Photo</a> </li>
				<li><a href="notifications.php">Notifications</a> </li>
			</ul>
		</li>
		-->
	</ul>
</div>